console.log('%c>> s23 Activity', 'color:pink;');
console.log('');

const trainer = {};
trainer.name = "Ash Ketchum";
trainer.age = 10;
trainer.pokemons = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];
trainer.friends = {
    hoenn: ["May", "Max"],
    kanto: ["Brock", "Misty"],
}
trainer.talk = function() {
    console.log('Pikachu! I choose you!');
}

console.log(trainer);
console.log('Result of dot notation:\n', trainer.name);
console.log('Result of square bracket notation:\n', trainer['pokemons']);
console.log('Result of talk method:');
trainer.talk();

function Pokemon(name, level) {
    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attack = level * 1.5;
    this.tackle = function(targetPokemon) {
        console.log(`${this.name} tackled ${targetPokemon.name}!`);
        targetPokemon.health -= this.attack;
        console.log(`${targetPokemon.name}'s health is now reduced to`, targetPokemon.health);
        if (targetPokemon.health <= 0) {
            targetPokemon.faint();
        }
    }
    this.faint = function() {
        console.log(`${this.name} has fainted.`);
    }
}

const myPikachu = new Pokemon('Pikachu', 12);
console.log(myPikachu);
const myGeodude = new Pokemon('Geodude', 8);
console.log(myGeodude);
const myMewtwo = new Pokemon('Mewtwo', 100);
console.log(myMewtwo);

console.log(' ');
myGeodude.tackle(myPikachu);
console.log(myPikachu);
console.log(' ');
myMewtwo.tackle(myGeodude);
console.log(myGeodude);